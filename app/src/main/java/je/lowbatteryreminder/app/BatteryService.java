package je.lowbatteryreminder.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import static android.view.WindowManager.*;

public class BatteryService extends Service {

    private static final String BTR_LVL_CHECK_INTENT_NAME = "do_battery_check";
    private final long TIME_INTERVAL = 1000 * 60 * 10;

    float mBatteryPct;
    private WindowManager mManager;
    private View mView;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        checkBattery();

        registerReceiver(mReceiver, new IntentFilter(BTR_LVL_CHECK_INTENT_NAME));
        alarmForBatteryCheck();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkBattery();
        }
    };

    private void alarmForBatteryCheck() {
        AlarmManager alarmManager = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Intent lIntent = new Intent(BTR_LVL_CHECK_INTENT_NAME);
        PendingIntent lPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, lIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                TIME_INTERVAL, lPendingIntent);
    }

    private void checkBattery() {
        IntentFilter lFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = getApplicationContext().registerReceiver(null, lFilter);

        try {
            int lLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int lScale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            if (lLevel != -1 && lScale != -1) {
                mBatteryPct = lLevel / (float) lScale;
                mBatteryPct *= 100;
            }

            int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

            if (mBatteryPct <= 25 && !usbCharge && !acCharge)  {
                showAlert();
            }
        } catch (NullPointerException e) {
            Log.e("Service", e.getMessage());
        }
    }

    private void showAlert() {
        mManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.aler_notification, null);

        Button button = (Button) mView.findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeAlert();
            }
        });

        ((TextView) mView.findViewById(R.id.remain)).setText(String.format(getString(R.string.remain), (int) mBatteryPct));

        MediaPlayer mPlayer = MediaPlayer.create(this, R.raw.low);

        LayoutParams lParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT,
                LayoutParams.TYPE_SYSTEM_ALERT,
                LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        lParams.gravity = Gravity.CENTER;

        mManager.addView(mView,lParams);

        AudioManager audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
        int volume = audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);


        mPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mediaPlayer) {
                mediaPlayer.stop();
            }
        });

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
        mPlayer.setVolume(volume, volume);
        mPlayer.start();
    }

    private void closeAlert() {
        mView.setVisibility(View.GONE);
    }
}
